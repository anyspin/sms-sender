<?php

namespace Anyspin\SmsSender\Contracts;

use Anyspin\SmsSender\Contracts\Smsable as SmsableContract;

interface SmsSender
{
    /**
     * Begin the process of smsing a smsable class instance.
     *
     * @param  mixed  $users
     * @return \Anyspin\SmsSender\PendingSms
     */
    public function to($users);

    /**
     * Send a new message using a view.
     *
     * @param  string|array|SmsableContract  $view
     * @param  array  $data
     * @param  \Closure|string  $callback
     * @return array
     */
    public function send($view, array $data = [], $callback = null);

    /**
     * Check a message.
     *
     * @param  string|array  $id
     * @return void
     */
    public function check($id);

}
