<?php

return [

    /*
    |--------------------------------------------------------------------------
    | SMS Driver
    |--------------------------------------------------------------------------
    |
    | Supported: "log", "smsru"
    |
    */

    'driver' => env('SMS_DRIVER', 'log'),

    /*
    |--------------------------------------------------------------------------
    | Global sender name
    |--------------------------------------------------------------------------
    |
    | You may wish for all SMSs sent by your application to be sent from
    | the same sender name.
    |
    */

    'from' => env('SMS_SENDER_NAME', null),

    /*
    |--------------------------------------------------------------------------
    | Log Channel
    |--------------------------------------------------------------------------
    |
    | If you are using the "log" driver, you may specify the logging channel
    | if you prefer to keep SMS separate from other log entries
    | for simpler reading. Otherwise, the default channel will be used.
    |
    */

    'log_channel' => env('SMS_LOG_CHANNEL'),

];
