<?php

namespace Anyspin\SmsSender;

use Illuminate\Support\Arr;
use Psr\Log\LoggerInterface;
use Illuminate\Support\Manager;
use GuzzleHttp\Client as HttpClient;
use Anyspin\SmsSender\Transport\LogTransport;
use Anyspin\SmsSender\Transport\SmsruTransport;

use Zelenin\SmsRu\Api as SmsruClient;
use Zelenin\SmsRu\Auth\ApiIdAuth as SmsruIdAuth;

class TransportManager extends Manager
{

    /**
     * Create an instance of the Smsru Transport driver.
     *
     * @return \Anyspin\SmsSender\Transport\SmsruTransport
     */
    protected function createSmsruDriver()
    {
        $config = $this->app['config']->get('services.smsru', []);

        return new SmsruTransport(
            $this->guzzle($config),
            $config['key']
        );
    }

    /**
     * Create an instance of the Log Swift Transport driver.
     *
     * @return \Anyspin\SmsSender\Transport\LogTransport
     */
    protected function createLogDriver()
    {
        return new LogTransport($this->app->make(LoggerInterface::class));
    }

    /**
     * Get a fresh Guzzle HTTP client instance.
     *
     * @param  array  $config
     * @return \GuzzleHttp\Client
     */
    protected function guzzle($config)
    {
        return new HttpClient(Arr::add(
            $config['guzzle'] ?? [], 'connect_timeout', 60
        ));
    }

    /**
     * Get the default SMS driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->app['config']['sms.driver'];
    }

    /**
     * Set the default SMS driver name.
     *
     * @param  string  $name
     * @return void
     */
    public function setDefaultDriver($name)
    {
        $this->app['config']['sms.driver'] = $name;
    }
}
