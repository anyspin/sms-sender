<?php

namespace Anyspin\SmsSender\Transport;

use Psr\Log\LoggerInterface;
use Anyspin\SmsSender\Message;
use Anyspin\SmsSender\SmsStatus;

class LogTransport extends Transport
{
    /**
     * The Logger instance.
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Create a new log transport instance.
     *
     * @param  \Psr\Log\LoggerInterface  $logger
     * @return void
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function send(Message $message)
    {
        $this->logger->debug(
            ($message->getFrom() ? 'Sender: ' . $message->getFrom() . '; ' : '')
            . ($message->getTo() ? 'Recipient: ' . implode(',', $message->getTo()) . '; ' : '')
            . 'Message: ' . $message->getContent()
        );

        $status = [];

        foreach ($message->getTo() as $phone)
        {
            $status[] = (new SmsStatus)
                ->setTo($phone)
                ->setId(uniqid())
                ->setStatus(SmsStatus::SENT);
        }

        return $status;
    }

    /**
     * {@inheritdoc}
     */
    public function check($id)
    {
        $ids = (array)$id;

        $status = [];

        foreach ($ids as $id)
        {
            $status[] = (new SmsStatus)
                ->setId($id)
                ->setCost(0.00)
                ->setDescription('Delivered')
                ->setStatus(SmsStatus::DELIVERED);
        }

        return $status;
    }

    /**
     * {@inheritdoc}
     */
    public function balance()
    {
        return 0;
    }

}
