<?php

namespace Anyspin\SmsSender;

class Message
{

    protected $from;

    protected $content;

    protected $to = [];

    /**
     * Set a sender name to the message.
     *
     * @param  string  $name
     * @return $this
     */
    public function from($name)
    {
        $this->from = $name;

        return $this;
    }

    /**
     * Add a recipient to the message.
     *
     * @param  string|array  $address
     * @return $this
     */
    public function to($phone)
    {
        $this->to = array_merge($this->to, (array)$phone);

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return (array)$this->to;
    }

    public function setContent($text)
    {
        $this->content = $text;
    }

    public function getContent()
    {
        return $this->content;
    }

}
