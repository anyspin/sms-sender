<?php

namespace Anyspin\SmsSender\Events;

use Anyspin\SmsSender\Message;

class MessageSending
{
    /**
     * The message instance.
     *
     * @var \Anyspin\SmsSender\Message
     */
    public $message;

    /**
     * The message data.
     *
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @param  \Anyspin\SmsSender\Message $message
     * @param  array  $data
     * @return void
     */
    public function __construct($message, $data = [])
    {
        $this->data = $data;
        $this->message = $message;
    }
}
