<?php

namespace Anyspin\SmsSender;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;

class SmsSenderServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSmsProvider();
        $this->registerSmsSender();
    }

    /**
     * Boot method.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/sms.php' => config_path('sms.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../config/sms.php', 'sms');
    }

    /**
     * Register the Anyspin SmsSender instance.
     *
     * @return void
     */
    protected function registerSmsSender()
    {
        $this->app->singleton('sms.sender', function ($app) {
            $config = $app->make('config')->get('sms');

            $smsSender = new SmsSender(
                $app['view'], $app['sms.provider'], $app['events']
            );

            if ($app->bound('queue'))
            {
                $smsSender->setQueue($app['queue']);
            }

            $senderName = Arr::get($config, 'from');

            if ($senderName)
            {
                $smsSender->alwaysFrom($senderName);
            }

            return $smsSender;
        });
    }

    /**
     * Register the SMS provider instance.
     *
     * @return void
     */
    public function registerSmsProvider()
    {
        $this->registerSmsTransport();

        $this->app->singleton('sms.provider', function ($app) {
            return new SmsProvider($app['sms.transport']->driver());
        });
    }

    /**
     * Register the SMS Transport instance.
     *
     * @return void
     */
    protected function registerSmsTransport()
    {
        $this->app->singleton('sms.transport', function ($app) {
            return new TransportManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'sms.sender',
            'sms.provider',
            'sms.transport',
        ];
    }
}
