# SMS Sender

SMS Laravel package

## Based on drivers

Currently implemented drivers:

- log - for debug purposes
- smsru - [sms.ru](https://sms.ru/)
- ... coming soon smsc.ru, smspilot.ru, etc

## Installation

Run this command from the Terminal:

```bash
composer require anyspin/sms-sender
```

If you would like to use facade, in your `app.php` config file add the following line to the aliases array if you want to use a facade:

```php
'Sms' => Anyspin\SmsSender\SmsFacade::class,
```

## Configuration

You can publish the default config file to `config/sms.php` with the artisan command:

```shell
php artisan vendor:publish --tag=config
```

## Usage

### Sending an SMS

```php
<?php

use Anyspin\SmsSender\SmsFacade as Sms;
use Anyspin\SmsSender\SmsStatus;

$result = Sms::content('Your code: 123456', function($message) {
    $message->to('+79991234567');

    // or multiple recipients:

    $message->to([
        '+79991234567',
        '+79991234568',
        '+79991234569',
    ]);
});

if ($result[0]->status === SmsStatus::SENT)
{
    // message is sent successfully
}

// or the other way:

if ($result[0]->sent())
{
    // message is sent successfully
}
```

The `$result` is an array with SmsStatus objects (one for each recipient), which contain the following properties:

- **id** - SMS external ID
- **to** - the phone number of recipient
- **cost** - the cost of this message (available only when checking, see below)
- **status** - SMS status, one of:
    - `SmsStatus::NONE`
    - `SmsStatus::SENT`
    - `SmsStatus::DELIVERING`
    - `SmsStatus::DELIVERED`
    - `SmsStatus::FAILED`

For convinience, SmsStatus also has some methods:

- **sent()** - return true if the message is sent successfully
- **delivering()** - return true if the message is delivering
- **delivered()** - return true if the message is delivered successfully
- **succeed()** - return true if the message is sent or delivering or delivered
- **failed()** - return true if the message is failed to deliver


### Checking the SMS status

```php
<?php

use Anyspin\SmsSender\SmsFacade as Sms;
use Anyspin\SmsSender\SmsStatus;

$result = Sms::check('123456');

if ($result[0]->status === SmsStatus::DELIVERED)
{
    // message is delivered successfully
}

// or the other way:

if ($result[0]->delivered())
{
    // message is delivered successfully
}
```

You can also check multiple IDs at once:

```php
$result = Sms::check(['123456', '123457', '123458']);
```

### Checking the balance on the external service

```php
<?php

use Anyspin\SmsSender\SmsFacade as Sms;

$balance = Sms::balance();
```
