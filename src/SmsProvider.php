<?php

namespace Anyspin\SmsSender;

class SmsProvider
{
    /** The Transport used to send messages */
    private $transport;

    /**
     * Create a new Provider using $transport for delivery.
     *
     * @param SmsTransport $transport
     */
    public function __construct(SmsTransport $transport)
    {
        $this->transport = $transport;
    }

    /**
     * Send the given Message.
     *
     * The return value is an array with sending status.
     *
     * @param \Anyspin\SmsSender\Message $message
     *
     * @return array
     */
    public function send(Message $message)
    {
        return $this->transport->send($message);
    }

    /**
     * Check a message.
     *
     * @param  string|array  $id
     * @return void
     */
    public function check($id)
    {
        return $this->transport->check($id);
    }

    /**
     * Get the balance in the SMS service.
     *
     * @return mixed
     */
    public function balance()
    {
        return $this->transport->balance();
    }

    /**
     * The Transport used to send messages.
     *
     * @return \Anyspin\SmsSender\SmsTransport
     */
    public function getTransport()
    {
        return $this->transport;
    }
}
