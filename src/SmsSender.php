<?php

namespace Anyspin\SmsSender;

use InvalidArgumentException;
use Illuminate\Support\HtmlString;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Anyspin\SmsSender\Contracts\SmsSender as SmsSenderContract;
use Illuminate\Contracts\Queue\Factory as QueueContract;
use Anyspin\SmsSender\Contracts\Smsable as SmsableContract;
use Anyspin\SmsSender\Contracts\SmsQueue as SmsQueueContract;

class SmsSender implements SmsSenderContract, SmsQueueContract
{
    use Macroable;

    /**
     * The view factory instance.
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $views;

    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher|null
     */
    protected $events;

    /**
     * 
     *
     * @var \Anyspin\SmsSender\SmsProvider|null
     */
    protected $provider;

    /**
     * The global from address and name.
     *
     * @var array
     */
    protected $from;

    /**
     * The queue implementation.
     *
     * @var \Illuminate\Contracts\Queue\Queue
     */
    protected $queue;

    /**
     * Array of failed recipients.
     *
     * @var array
     */
    protected $failedRecipients = [];

    /**
     * Create a new SmsSender instance.
     *
     * @param  \Illuminate\Contracts\View\Factory  $views
     * @param  \Anyspin\SmsSender\SmsProvider  $provider
     * @param  \Illuminate\Contracts\Events\Dispatcher|null  $events
     * @return void
     */
    public function __construct(Factory $views, SmsProvider $provider, Dispatcher $events = null)
    {
        $this->views = $views;
        $this->provider = $provider;
        $this->events = $events;
    }

    /**
     * Set the global from address and name.
     *
     * @param  string  $name
     * @return void
     */
    public function alwaysFrom($name)
    {
        $this->from = $name;
    }

    /**
     * Begin the process of smsing a smsable class instance.
     *
     * @param  mixed  $phones
     * @return \Anyspin\SmsSender\PendingSms
     */
    public function to($phones)
    {
        return (new PendingSms($this))->to($phones);
    }

    /**
     * Begin the process of smsing a smsable class instance.
     *
     * @param  string  $from
     * @return \Anyspin\SmsSender\PendingSms
     */
    public function from($from)
    {
        return (new PendingSms($this))->from($from);
    }

    /**
     * Send a new message with only a raw text.
     *
     * @param  string  $text
     * @param  mixed  $callback
     * @return void
     */
    public function raw($text, $callback)
    {
        return $this->send(new HtmlString($text), [], $callback);
    }

    /**
     * Send a new message with only a raw text.
     *
     * @param  string  $text
     * @param  mixed  $callback
     * @return void
     */
    public function content($text, $callback)
    {
        return $this->send(new HtmlString($text), [], $callback);
    }

    /**
     * Render the given message as a view.
     *
     * @param  string|array  $view
     * @param  array  $data
     * @return string
     */
    public function render($view, array $data = [])
    {
        $view = $this->parseView($view);

        $data['message'] = $this->createMessage();

        return $this->renderView($view, $data);
    }

    /**
     * Send a new message using a view.
     *
     * @param  string|array|SmsableContract  $view
     * @param  array  $data
     * @param  \Closure|string  $callback
     * @return void
     */
    public function send($view, array $data = [], $callback = null)
    {
        if ($view instanceof SmsableContract)
        {
            return $this->sendSmsable($view);
        }

        $view = $this->parseView($view);

        $data['message'] = $message = $this->createMessage();

        if (is_callable($callback))
        {
            call_user_func($callback, $message);
        }

        $this->addContent($message, $view, $data);

        $result = null;

        if ($this->shouldSendMessage($message, $data))
        {
            $result = $this->sendMessage($message);

            $this->dispatchSentEvent($message, $data);
        }

        return $result;
    }

    /**
     * Check a message.
     *
     * @param  string|array  $id
     * @return void
     */
    public function check($id)
    {
        return $this->provider->check($id);
    }

    /**
     * Get the balance in the SMS service.
     *
     * @return mixed
     */
    public function balance()
    {
        return $this->provider->balance();
    }

    /**
     * Send the given smsable.
     *
     * @param  \Anyspin\SmsSender\Contracts\Smsable  $smsable
     * @return mixed
     */
    protected function sendSmsable(SmsableContract $smsable)
    {
        return $smsable instanceof ShouldQueue
                ? $smsable->queue($this->queue) : $smsable->send($this);
    }

    /**
     * Parse the given view name.
     *
     * @param  string  $view
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected function parseView($view)
    {
        return $view;
    }

    /**
     * Add the content to a given message.
     *
     * @param  \Anyspin\SmsSender\Message  $message
     * @param  string  $view
     * @param  string  $plain
     * @param  string  $raw
     * @param  array  $data
     * @return void
     */
    protected function addContent($message, $view, $data)
    {
        if (isset($view))
        {
            $message->setContent($this->renderView($view, $data));
        }
    }

    /**
     * Render the given view.
     *
     * @param  string  $view
     * @param  array  $data
     * @return string
     */
    protected function renderView($view, $data)
    {
        return $view instanceof Htmlable
                        ? $view->toHtml()
                        : $this->views->make($view, $data)->render();
    }

    /**
     * Queue a new e-sms message for sending.
     *
     * @param  string|array|SmsableContract  $view
     * @param  string|null  $queue
     * @return mixed
     */
    public function queue($view, $queue = null)
    {
        if (! $view instanceof SmsableContract)
        {
            throw new InvalidArgumentException('Only smsables may be queued.');
        }

        return $view->queue(is_null($queue) ? $this->queue : $queue);
    }

    /**
     * Queue a new e-sms message for sending on the given queue.
     *
     * @param  string  $queue
     * @param  string|array  $view
     * @return mixed
     */
    public function onQueue($queue, $view)
    {
        return $this->queue($view, $queue);
    }

    /**
     * Queue a new e-sms message for sending on the given queue.
     *
     * This method didn't match rest of framework's "onQueue" phrasing. Added "onQueue".
     *
     * @param  string  $queue
     * @param  string|array  $view
     * @return mixed
     */
    public function queueOn($queue, $view)
    {
        return $this->onQueue($queue, $view);
    }

    /**
     * Queue a new e-sms message for sending after (n) seconds.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  string|array|SmsableContract  $view
     * @param  string|null  $queue
     * @return mixed
     */
    public function later($delay, $view, $queue = null)
    {
        if (! $view instanceof SmsableContract)
        {
            throw new InvalidArgumentException('Only smsables may be queued.');
        }

        return $view->later($delay, is_null($queue) ? $this->queue : $queue);
    }

    /**
     * Queue a new e-sms message for sending after (n) seconds on the given queue.
     *
     * @param  string  $queue
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  string|array  $view
     * @return mixed
     */
    public function laterOn($queue, $delay, $view)
    {
        return $this->later($delay, $view, $queue);
    }

    /**
     * Create a new message instance.
     *
     * @return \Anyspin\SmsSender\Message
     */
    protected function createMessage()
    {
        $message = new Message;

        if ( ! empty($this->from))
        {
            $message->from($this->from);
        }

        return $message;
    }

    /**
     * Send a Message instance.
     *
     * @param  Message  $message
     * @return void
     */
    protected function sendMessage($message)
    {
        return $this->provider->send($message, $this->failedRecipients);
    }

    /**
     * Determines if the message can be sent.
     *
     * @param  Message  $message
     * @param  array  $data
     * @return bool
     */
    protected function shouldSendMessage($message, $data = [])
    {
        if ( ! $this->events)
        {
            return true;
        }

        return $this->events->until(
            new Events\MessageSending($message, $data)
        ) !== false;
    }

    /**
     * Dispatch the message sent event.
     *
     * @param  \Anyspin\SmsSender\Message  $message
     * @param  array  $data
     * @return void
     */
    protected function dispatchSentEvent($message, $data = [])
    {
        if ($this->events)
        {
            $this->events->dispatch(
                new Events\MessageSent($message, $data)
            );
        }
    }

    /**
     * Get the view factory instance.
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public function getViewFactory()
    {
        return $this->views;
    }

    /**
     * Get the array of failed recipients.
     *
     * @return array
     */
    public function failures()
    {
        return $this->failedRecipients;
    }

    /**
     * Set the queue manager instance.
     *
     * @param  \Illuminate\Contracts\Queue\Factory  $queue
     * @return $this
     */
    public function setQueue(QueueContract $queue)
    {
        $this->queue = $queue;

        return $this;
    }
}
