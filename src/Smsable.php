<?php

namespace Anyspin\SmsSender;

use ReflectionClass;
use ReflectionProperty;
use BadMethodCallException;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\Container\Container;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\Queue\Factory as Queue;
use Illuminate\Contracts\Translation\Translator;
use Anyspin\SmsSender\Contracts\SmsSender as SmsSenderContract;
use Anyspin\SmsSender\Contracts\Smsable as SmsableContract;

class Smsable implements SmsableContract, Renderable
{
    /**
     * The locale of the message.
     *
     * @var string
     */
    public $locale;

    /**
     * The sender name of the message.
     *
     * @var array
     */
    public $from = null;

    /**
     * The "to" recipients of the message.
     *
     * @var array
     */
    public $to = [];

    /**
     * The text to use for the message.
     *
     * @var string
     */
    protected $text;

    /**
     * The view to use for the message.
     *
     * @var string
     */
    public $view;

    /**
     * The view data for the message.
     *
     * @var array
     */
    public $viewData = [];

    /**
     * The callbacks for the message.
     *
     * @var array
     */
    public $callbacks = [];

    /**
     * Send the message using the given smsSender.
     *
     * @param  \Anyspin\SmsSender\Contracts\SmsSender  $smsSender
     * @return void
     */
    public function send(SmsSenderContract $smsSender)
    {
        $translator = Container::getInstance()->make(Translator::class);

        $this->withLocale($this->locale, $translator, function () use ($smsSender) {
            Container::getInstance()->call([$this, 'build']);

            $smsSender->send($this->buildView(), $this->buildViewData(), function ($message) {
                $this->buildFrom($message)
                     ->buildRecipients($message)
                     ->runCallbacks($message);
            });
        });
    }

    /**
     * Queue the message for sending.
     *
     * @param  \Illuminate\Contracts\Queue\Factory  $queue
     * @return mixed
     */
    public function queue(Queue $queue)
    {
        if (property_exists($this, 'delay')) {
            return $this->later($this->delay, $queue);
        }

        $connection = property_exists($this, 'connection') ? $this->connection : null;

        $queueName = property_exists($this, 'queue') ? $this->queue : null;

        return $queue->connection($connection)->pushOn(
            $queueName ?: null, new SendQueuedSmsable($this)
        );
    }

    /**
     * Deliver the queued message after the given delay.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  \Illuminate\Contracts\Queue\Factory  $queue
     * @return mixed
     */
    public function later($delay, Queue $queue)
    {
        $connection = property_exists($this, 'connection') ? $this->connection : null;

        $queueName = property_exists($this, 'queue') ? $this->queue : null;

        return $queue->connection($connection)->laterOn(
            $queueName ?: null, $delay, new SendQueuedSmsable($this)
        );
    }

    /**
     * Render the smsable into a view.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        Container::getInstance()->call([$this, 'build']);

        return Container::getInstance()->make('sms.sender')->render(
            $this->buildView(), $this->buildViewData()
        );
    }

    /**
     * Build the view for the message.
     *
     * @return string
     */
    protected function buildView()
    {
        if (isset($this->text))
        {
            return new HtmlString($this->text);
        }

        return $this->view;
    }

    /**
     * Build the view data for the message.
     *
     * @return array
     */
    public function buildViewData()
    {
        $data = $this->viewData;

        foreach ((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if ($property->getDeclaringClass()->getName() != self::class) {
                $data[$property->getName()] = $property->getValue($this);
            }
        }

        return $data;
    }

    /**
     * Add the sender to the message.
     *
     * @param  \Anyspin\SmsSender\Message  $message
     * @return $this
     */
    protected function buildFrom($message)
    {
        if (! empty($this->from))
        {
            $message->from($this->from);
        }

        return $this;
    }

    /**
     * Add all of the recipients to the message.
     *
     * @param  \Anyspin\SmsSender\Message  $message
     * @return $this
     */
    protected function buildRecipients($message)
    {
        foreach ($this->to as $recipient)
        {
            $message->to($recipient);
        }

        return $this;
    }

    /**
     * Run the callbacks for the message.
     *
     * @param  \Anyspin\SmsSender\Message  $message
     * @return $this
     */
    protected function runCallbacks($message)
    {
        foreach ($this->callbacks as $callback)
        {
            $callback($message);
        }

        return $this;
    }

    /**
     * Set the sender of the message.
     *
     * @param  string|null  $name
     * @return $this
     */
    public function from($name)
    {
        $this->from = $name;
        return $this;
    }

    /**
     * Determine if the given sender name is actually sender name.
     *
     * @param  string|null  $name
     * @return bool
     */
    public function isFrom($name)
    {
        return $this->from === $name;
    }

    /**
     * Set the recipients of the message.
     *
     * @param  string  $phone
     * @return $this
     */
    public function to($phone)
    {
        $this->to[] = $phone;
        return $this;
    }

    /**
     * Determine if the given recipient is set on the smsable.
     *
     * @param  string  $phone
     * @return bool
     */
    public function hasTo($phone)
    {
        return in_array($phone, $this->to);
    }

    /**
     * Set the view and view data for the message.
     *
     * @param  string  $view
     * @param  array  $data
     * @return $this
     */
    public function view($view, array $data = [])
    {
        $this->view = $view;
        $this->viewData = array_merge($this->viewData, $data);

        return $this;
    }

    /**
     * Set the plain text view for the message.
     *
     * @param  string  $text
     * @param  array  $data
     * @return $this
     */
    public function text($text, array $data = [])
    {
        $this->text = $text;
        $this->viewData = array_merge($this->viewData, $data);

        return $this;
    }

    /**
     * Set the view data for the message.
     *
     * @param  string|array  $key
     * @param  mixed   $value
     * @return $this
     */
    public function with($key, $value = null)
    {
        if (is_array($key)) {
            $this->viewData = array_merge($this->viewData, $key);
        } else {
            $this->viewData[$key] = $value;
        }

        return $this;
    }

    /**
     * Dynamically bind parameters to the message.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return $this
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        if (Str::startsWith($method, 'with')) {
            return $this->with(Str::snake(substr($method, 4)), $parameters[0]);
        }

        throw new BadMethodCallException(sprintf(
            'Method %s::%s does not exist.', static::class, $method
        ));
    }
}
