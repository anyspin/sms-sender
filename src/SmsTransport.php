<?php

namespace Anyspin\SmsSender;

interface SmsTransport
{
    /**
     * Send the given Message.
     *
     * @param Anyspin\SmsSender\Message $message
     *
     * @return array
     */
    public function send(Message $message);

    /**
     * Check the given message id.
     *
     * @param string|array $id
     *
     * @return array
     */
    public function check($id);

    /**
     * Get the balance in the SMS service.
     *
     * @return mixed
     */
    public function balance();
}
