<?php

namespace Anyspin\SmsSender;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Anyspin\SmsSender\SmsSender
 */
class SmsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sms.sender';
    }
}
