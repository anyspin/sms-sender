<?php

namespace Anyspin\SmsSender;

use Anyspin\SmsSender\Contracts\SmsSender as SmsSenderContract;
use Anyspin\SmsSender\Contracts\Smsable as SmsableContract;

class SendQueuedSmsable
{
    /**
     * The smsable message instance.
     *
     * @var Smsable
     */
    public $smsable;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout;

    /**
     * Create a new job instance.
     *
     * @param  \Anyspin\SmsSender\Contracts\Smsable  $smsable
     * @return void
     */
    public function __construct(SmsableContract $smsable)
    {
        $this->smsable = $smsable;
        $this->tries = property_exists($smsable, 'tries') ? $smsable->tries : null;
        $this->timeout = property_exists($smsable, 'timeout') ? $smsable->timeout : null;
    }

    /**
     * Handle the queued job.
     *
     * @param  \Anyspin\SmsSender\Contracts\SmsSender  $smsSender
     * @return void
     */
    public function handle(SmsSenderContract $smsSender)
    {
        $this->smsable->send($smsSender);
    }

    /**
     * Get the display name for the queued job.
     *
     * @return string
     */
    public function displayName()
    {
        return get_class($this->smsable);
    }

    /**
     * Call the failed method on the smsable instance.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function failed($e)
    {
        if (method_exists($this->smsable, 'failed')) {
            $this->smsable->failed($e);
        }
    }

    /**
     * Prepare the instance for cloning.
     *
     * @return void
     */
    public function __clone()
    {
        $this->smsable = clone $this->smsable;
    }
}
