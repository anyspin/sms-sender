<?php

namespace Anyspin\SmsSender;

class SmsStatus
{
    const STATUS_NONE       = null;
    const STATUS_SENT       = 'sent';
    const STATUS_DELIVERED  = 'delivered';
    const STATUS_DELIVERING = 'delivering';
    const STATUS_READ       = 'read';
    const STATUS_FAILED     = 'failed';

    const NONE       = null;
    const SENT       = 'sent';
    const DELIVERED  = 'delivered';
    const DELIVERING = 'delivering';
    const READ       = 'read';
    const FAILED     = 'failed';

    public $id;

    public $status;

    public $code;

    public $description;

    public $to;

    public $cost;

    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function markAsSent()
    {
        $this->setStatus(static::SENT);
    }

    public function markAsDelivered()
    {
        $this->setStatus(static::DELIVERED);
    }

    public function markAsDelivering()
    {
        $this->setStatus(static::DELIVERING);
    }

    public function markAsFailed()
    {
        $this->setStatus(static::FAILED);
    }

    public function sent()
    {
        return $this->status === static::SENT;
    }

    public function delivered()
    {
        return $this->status === static::DELIVERED;
    }

    public function delivering()
    {
        return $this->status === static::DELIVERING;
    }

    public function succeed()
    {
        return $this->status === static::SENT
            || $this->status === static::DELIVERED
            || $this->status === static::DELIVERING
            || $this->status === static::READ;
    }

    public function failed()
    {
        return $this->status === static::FAILED;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }
}
