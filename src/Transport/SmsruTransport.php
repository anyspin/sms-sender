<?php

namespace Anyspin\SmsSender\Transport;

use GuzzleHttp\ClientInterface;
use Anyspin\SmsSender\Message;
use Anyspin\SmsSender\SmsStatus;

use Log;

class SmsruTransport extends Transport
{
    /**
     * Guzzle client instance.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    /**
     * The SmsRu API key.
     *
     * @var string
     */
    protected $key;

    /**
     * The SmsRu API end-point.
     *
     * @var string
     */
    protected $url = 'https://sms.ru/';

    /**
     * Create a new SmsRu transport instance.
     *
     * @param  \GuzzleHttp\ClientInterface  $client
     * @param  string  $key
     * @return void
     */
    public function __construct(ClientInterface $client, $key)
    {
        $this->key = $key;
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function send(Message $message, &$failedRecipients = null)
    {
        $recipient = implode(',', $message->getTo());

        $response = $this->client->post($this->url . 'sms/send', [
            'form_params' => [
                'api_id' => $this->key,
                'to'     => $recipient,
                'msg'    => $message->getContent(),
                'json'   => 1,
            ],
        ]);

        $result = json_decode( $response->getBody() );

        $status = [];

        if ($response->getStatusCode() === 200)
        {
            switch ($result->status_code)
            {
                case 100:
                    if (!empty($result->sms))
                    {
                        foreach ($result->sms as $phone => $sms)
                        {
                            $status[] = (new SmsStatus)
                                ->setTo($phone)
                                ->setId($sms->sms_id)
                                ->setCode($sms->status_code)
                                ->setStatus($this->parseStatus($sms->status_code));
                        }
                    }
                    break;

                case 200:
                    Log::critical('SmsRu: Invalid API key');
                    break;
            }
        }

        return $status;
    }

    /**
     * {@inheritdoc}
     */
    public function check($id)
    {
        $ids = implode(',', (array)$id);

        $response = $this->client->post($this->url . 'sms/status', [
            'form_params' => [
                'api_id' => $this->key,
                'id'     => $ids,
                'json'   => 1,
            ],
        ]);

        $result = json_decode( $response->getBody() );

        $status = [];

        if ($response->getStatusCode() === 200)
        {
            switch ($result->status_code)
            {
                case 100:
                    foreach ($result->sms as $id => $sms)
                    {
                        $status[] = (new SmsStatus)
                            ->setId($id)
                            ->setCost($sms->cost)
                            ->setCode($sms->status_code)
                            ->setDescription($sms->status_text)
                            ->setStatus($this->parseStatus($sms->status_code));
                    }
                    break;

                case 200:
                    Log::critical('SmsRu: Invalid API key');
                    break;
            }
        }

        return $status;
    }

    /**
     * {@inheritdoc}
     */
    public function balance()
    {
        $response = $this->client->post($this->url . 'my/balance', [
            'form_params' => [
                'api_id' => $this->key,
                'json'   => 1,
            ],
        ]);

        $result = json_decode( $response->getBody() );

        $balance = [];

        if ($response->getStatusCode() === 200)
        {
            switch ($result->status_code)
            {
                case 100:
                    $balance = $result->balance;
                    break;

                case 200:
                    Log::critical('SmsRu: Invalid API key');
                    break;
            }
        }

        return $balance;
    }

    public function parseStatus($code)
    {
        switch ($code)
        {
            case 100:
                return SmsStatus::SENT;

            case 101:
            case 102:
                return SmsStatus::DELIVERING;
                break;

            case 103:
                return SmsStatus::DELIVERED;
                break;

            case 110:
                return SmsStatus::READ;
                break;

            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 150:
                return SmsStatus::FAILED;
                break;

            default:
                return SmsStatus::FAILED;
                break;
        }
    }

}
