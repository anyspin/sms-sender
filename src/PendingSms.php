<?php

namespace Anyspin\SmsSender;

use Illuminate\Contracts\Queue\ShouldQueue;

class PendingSms
{
    /**
     * The smsSender instance.
     *
     * @var \Anyspin\SmsSender\SmsSender
     */
    protected $smsSender;

    /**
     * The locale of the message.
     *
     * @var array
     */
    protected $locale;

    /**
     * The "to" recipients of the message.
     *
     * @var array
     */
    protected $to = [];

    /**
     * Create a new smsable smsSender instance.
     *
     * @param  \Anyspin\SmsSender\SmsSender  $smsSender
     * @return void
     */
    public function __construct(SmsSender $smsSender)
    {
        $this->smsSender = $smsSender;
    }

    /**
     * Set the recipients of the message.
     *
     * @param  mixed  $users
     * @return $this
     */
    public function to($users)
    {
        $this->to = $users;

        return $this;
    }

    /**
     * Send a new smsable message instance.
     *
     * @param  \Anyspin\SmsSender\Smsable  $smsable
     * @return mixed
     */
    public function send(Smsable $smsable)
    {
        if ($smsable instanceof ShouldQueue)
        {
            return $this->queue($smsable);
        }

        return $this->smsSender->send($this->fill($smsable));
    }

    /**
     * Send a smsable message immediately.
     *
     * @param  \Anyspin\SmsSender\Smsable  $smsable
     * @return mixed
     */
    public function sendNow(Smsable $smsable)
    {
        return $this->smsSender->send($this->fill($smsable));
    }

    /**
     * Push the given smsable onto the queue.
     *
     * @param  \Anyspin\SmsSender\Smsable  $smsable
     * @return mixed
     */
    public function queue(Smsable $smsable)
    {
        $smsable = $this->fill($smsable);

        if (isset($smsable->delay)) {
            return $this->smsSender->later($smsable->delay, $smsable);
        }

        return $this->smsSender->queue($smsable);
    }

    /**
     * Deliver the queued message after the given delay.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  \Anyspin\SmsSender\Smsable  $smsable
     * @return mixed
     */
    public function later($delay, Smsable $smsable)
    {
        return $this->smsSender->later($delay, $this->fill($smsable));
    }

    /**
     * Populate the smsable with the addresses.
     *
     * @param  \Anyspin\SmsSender\Smsable  $smsable
     * @return \Anyspin\SmsSender\Smsable
     */
    protected function fill(Smsable $smsable)
    {
        return $smsable->to($this->to);
    }
}
