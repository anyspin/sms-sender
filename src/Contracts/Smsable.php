<?php

namespace Anyspin\SmsSender\Contracts;

use Illuminate\Contracts\Queue\Factory as Queue;

interface Smsable
{
    /**
     * Send the message using the given smsSender.
     *
     * @param  \Anyspin\SmsSender\Contracts\SmsSender  $smsSender
     * @return void
     */
    public function send(SmsSender $smsSender);

    /**
     * Queue the given message.
     *
     * @param  \Illuminate\Contracts\Queue\Factory  $queue
     * @return mixed
     */
    public function queue(Queue $queue);

    /**
     * Deliver the queued message after the given delay.
     *
     * @param  \DateTime|int  $delay
     * @param  \Illuminate\Contracts\Queue\Factory  $queue
     * @return mixed
     */
    public function later($delay, Queue $queue);
}
