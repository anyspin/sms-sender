<?php

namespace Anyspin\SmsSender\Contracts;

interface SmsQueue
{
    /**
     * Queue a new message for sending.
     *
     * @param  string|array|\Anyspin\SmsSender\Contracts\Smsable  $view
     * @param  string  $queue
     * @return mixed
     */
    public function queue($view, $queue = null);

    /**
     * Queue a new message for sending after (n) seconds.
     *
     * @param  \DateTimeInterface|\DateInterval|int  $delay
     * @param  string|array|\Anyspin\SmsSender\Contracts\Smsable  $view
     * @param  string  $queue
     * @return mixed
     */
    public function later($delay, $view, $queue = null);
}
